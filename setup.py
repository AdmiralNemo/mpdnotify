from setuptools import find_packages, setup

setup(
    name='mpdnotify',
    version='0.1',
    description='Send libnotify notifications for MPD',
    author='Dustin C. Hatch',
    author_email='dustin@hatch.name',
    url='http://dustin.hatch.name/',
    license='',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    package_data={'': ['mpd-logo.png']},
    entry_points={
        'console_scripts': [
            'mpdnotify=mpdnotify:main',
        ],
    },
)
