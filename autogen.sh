#!/bin/sh

srcdir=$(dirname $0)
test -z "${srcdir}" && srcdir=.

for f in COPYING NEWS README AUTHORS ChangeLog; do
    test -f ${srcdir}/$f || touch ${srcdir}/$f
done

rm -f .version
autoreconf -v --install ${srcdir} || exit $?

test -z "${NOCONFIGURE}" && "${srcdir}"/configure "$@"
