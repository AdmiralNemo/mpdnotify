using Mpd;

namespace MpdNotify {

    class Notifier {
        private const int RECONNECT_INTERVAL = 1;

        private string host;
        private int port;
        private string? password;
        private Connection conn;
        private Notify.Notification? notif = null;

        public Notifier(string host, int port = 0, string? password = null) {
            this.host = host;
            this.port = port;
            this.password = password;
        }

        public void start() {
            if (!connect()) {
                stderr.printf("Failed to connect to %s, will retry " +
                              "in %d second(s)\n", host, RECONNECT_INTERVAL);
                reconnect();
            } else {
                notify();
                conn.send_idle_mask(Mpd.Idle.PLAYER);
            }
        }

        private bool connect() {
            conn = new Connection(host, port);
            if (conn.error != Mpd.Error.SUCCESS) {
                return false;
            }
            if (password != null) {
                conn.run_password(password);
            }
            var chan = new IOChannel.unix_new(conn.fd);
            chan.add_watch(IOCondition.IN, changed);
            return true;
        }

        private void reconnect() {
            var timer = new TimeoutSource(RECONNECT_INTERVAL * 1000);
            timer.set_callback(() => {
                if (!connect()) {
                    return true;
                }
                notify();
                conn.send_idle_mask(Mpd.Idle.PLAYER);
                return false;
            });
            timer.attach(null);
        }

        private bool changed(IOChannel src, IOCondition cond) {
            var res = conn.recv_idle(false);
            if (res == 0) {
                send_notification(
                    "Disconnected",
                    "Lost connection to MPD, reconnecting...",
                    "audio-x-generic"
                );
                reconnect();
                return false;
            } else {
                notify();
                conn.send_idle_mask(Mpd.Idle.PLAYER);
                return true;
            }
        }

        private void send_notification(string summary, string? body = null,
                string? icon = null, int timeout = 2000) {
            string thebody = null;
            if (body != null) {
                thebody = body.replace("&", "&amp;");
            }
            if (notif == null) {
                if (Notify.is_initted()) {
                    Notify.uninit();
                }
                Notify.init("mpdnotify");
                notif = new Notify.Notification(summary, thebody, icon);
                notif.set_timeout(timeout);
                notif.closed.connect(() => {
                    notif = null;
                });
            } else {
                notif.update(summary, thebody, icon);
            }
            try {
                notif.show();
            } catch {
                stderr.printf("Failed to show notification!\n");
                notif = null;
            }
        }

        private void notify() {
            string title;
            string artist;
            string album;
            string duration;
            string summary;
            string body;

            var status = conn.run_status();
            var song = conn.run_current_song();

            title = song.get_tag(TagType.TITLE);
            switch (status.state) {
            case State.PAUSE:
                summary = "%s (Paused)".printf(title);
                break;
            case State.STOP:
                summary = "%s (Stopped)".printf(title);
                break;
            default:
                summary = title;
                break;
            }

            artist = song.get_tag(TagType.ARTIST);
            album = song.get_tag(TagType.ALBUM);
            duration = "%u:%02u".printf(song.duration / 60,
                                        song.duration % 60);

            body = "<i>from</i> %s <i>by</i> %s (%s)".printf(
                album,
                artist,
                duration
            );

            var art = new AlbumArt(artist, album);
            string icon;
            if (art.cached) {
                icon = art.filename;
            } else {
                icon = "audio-x-generic";
                art.download_complete.connect((s) => {
                    if (s && notif != null) {
                        conn.run_noidle();
                        notify();
                        conn.send_idle_mask(Mpd.Idle.PLAYER);
                    }
                });
                new Thread<void*>(null, (ThreadFunc) art.fetch);
            }

            send_notification(summary, body, icon);
        }

    }

}
