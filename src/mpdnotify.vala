namespace MpdNotify {
    public static const string DEFAULT_HOST = "localhost";
    public static const int DEFAULT_PORT = 6600;

    private static string host;
    private static int port;
    private static bool no_fork;

    private const OptionEntry[] options = {
        {"host", 'h', 0, OptionArg.STRING, ref host,
            "MPD server hostname/address", "HOST" },
        {"port", 'p', 0, OptionArg.INT, ref port,
            "MPD server port", "PORT" },
        {"no-fork", 'f', 0, OptionArg.NONE, ref no_fork,
            "Do not fork into the background", null },
        { null }
    };

    private static void parse_args(string[] args) throws OptionError {
        host = Environment.get_variable("MPD_HOST");
        if (host == null) {
            host = DEFAULT_HOST;
        }
        string port_s = Environment.get_variable("MPD_PORT");
        if (port_s == null) {
            port = DEFAULT_PORT;
        } else {
            int p;
            port_s.scanf("%d", out p);
            port = p;
        }

        var ctx = new OptionContext(
            "- Send desktop notifications for MPD events");
        ctx.set_help_enabled(true);
        ctx.add_main_entries(options, null);
        ctx.parse(ref args);
    }

    void daemonize() {
        Posix.pid_t pid;
        pid = Posix.fork();
        if (pid < 0) {
            stderr.printf("Unable to fork: %s", Posix.strerror(errno));
            Posix.exit(1);
        } else if (pid > 0) {
            Posix.exit(0);
        }
        Posix.pid_t sid = Posix.setsid();
        if (sid < 0) {
            stderr.printf("Unable to create new session: %s",
                Posix.strerror(errno));
            Posix.exit(1);
        }
        pid = Posix.fork();
        if (pid < 0) {
            stderr.printf("Unable to fork: %s", Posix.strerror(errno));
            Posix.exit(1);
        } else if (pid > 0) {
            Posix.exit(0);
        }
    }

    int main(string[] args) {
        try {
            parse_args(args);
        } catch (OptionError e) {
            stderr.printf("Error: %s\n", e.message);
            return 2;
        }

        if (!no_fork) {
            daemonize();
        }

        var loop = new MainLoop();
        Unix.signal_add(2, () => {
            loop.quit();
            return false;
        });

        string _host;
        string _password;
        var split_host = host.split("@", 2);
        if (split_host.length == 2) {
            _host = split_host[1];
            _password = split_host[0];
        } else {
            _host = split_host[0];
            _password = null;
        }
        var notifier = new Notifier(_host, port, _password);
        notifier.start();
        loop.run();
        return 0;
    }

}
